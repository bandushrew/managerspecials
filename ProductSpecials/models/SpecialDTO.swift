//
//  ManagerSpecial.swift
//  ProductSpecials
//
//  Created by Andrew Bush on 13/05/21.
//

import Foundation

struct SpecialDTO {
    let imageURL: String
    let width: Int
    let height: Int
    let displayName: String
    let originalPrice: String
    let price: String
    let listIndex: Int
}
