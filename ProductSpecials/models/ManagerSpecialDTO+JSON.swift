//
//  ManagerSpecialDTO+JSON.swift
//  ProductSpecials
//
//  Created by Andrew Bush on 16/05/21.
//

import Foundation

extension ManagerSpecialDTO {
    init?(json: Dictionary<String, Any>) {
        guard let canvasUnit = json["canvasUnit"] as? Int else {
            return nil
        }
        guard let rawSpecials = json["managerSpecials"]  as? [AnyObject] else {
            return nil
        }
        var specials: [SpecialDTO] = []
        for (index, rawSpecial) in rawSpecials.enumerated() {
            if let rawSpecial = rawSpecial as? Dictionary<String, AnyObject>,
               let special = SpecialDTO(json: rawSpecial, listIndex: index) {
                specials.append(special)
            }
        }
        self.canvasUnit = canvasUnit
        self.specials = specials
    }
}
