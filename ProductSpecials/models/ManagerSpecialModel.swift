//
//  ManagerSpecialModel.swift
//  ProductSpecials
//
//  Created by Andrew Bush on 15/05/21.
//

import Foundation
import UIKit
struct ManagerSpecialModel {
    let canvasUnit: Int
    let specials: [SpecialModel]
    
    func orderedSpecials(viewWidth: CGFloat) -> [ModelContainer] {
        var orderedSpecials: [ModelContainer] = []
        var currentTotal: CGFloat = 0
        var modelContainer: ModelContainer = ModelContainer(productSpecials: [])
        for special in specials {
            if(special.ui.size.width + currentTotal<=viewWidth) {
                modelContainer.productSpecials.append(special)
                currentTotal=currentTotal + special.ui.size.width
            } else {
                orderedSpecials.append(modelContainer)
                modelContainer = ModelContainer(productSpecials:[special])
                currentTotal = special.ui.size.width
            }
        }
        if(modelContainer.productSpecials.count>0) {
            orderedSpecials.append(modelContainer)
        }
        return orderedSpecials
    }
    static func frame(from special: SpecialDTO,
                      canvasUnit: Int,
                      viewWidth: CGFloat ) -> CGSize {
        let size=CGSize(width: viewWidth/CGFloat(canvasUnit) * CGFloat(special.width),
                        height: viewWidth/CGFloat(canvasUnit) * CGFloat(special.height))
        return size
    }
}

struct ModelContainer: Hashable, Identifiable {
    let id = UUID()
    var productSpecials: [SpecialModel]
    static func == (lhs: ModelContainer, rhs: ModelContainer) -> Bool {
        return lhs.id == rhs.id
    }
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
