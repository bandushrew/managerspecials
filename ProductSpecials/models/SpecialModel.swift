//
//  ProductFrame.swift
//  ProductSpecials
//
//  Created by Andrew Bush on 13/05/21.
//

import CoreGraphics
import UIKit

struct SpecialModel: Hashable, Identifiable {
    let id = UUID()
    let ui: SpecialUI
    let detail: SpecialDetail
    let listIndex: Int
    static func == (lhs: SpecialModel, rhs: SpecialModel) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
    func hash(into hasher: inout Hasher) {
        hasher.combine(detail.displayName)
        hasher.combine(detail.originalPrice)
        hasher.combine(detail.price)
        hasher.combine(listIndex)
    }
}

struct SpecialUI {
    let size: CGSize
    var image: UIImage
}

struct SpecialDetail {
    let displayName: String
    let originalPrice: String
    let price: String
}
