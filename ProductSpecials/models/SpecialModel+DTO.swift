//
//  SpecialModel+DTO.swift
//  ProductSpecials
//
//  Created by Andrew Bush on 15/05/21.
//

import Foundation
import UIKit
extension SpecialModel {
    init?(dto: SpecialDTO, size: CGSize, imageCache:Dictionary<String, UIImage>) {
        guard let image = imageCache[dto.imageURL] else {
            return nil
        }
        let detail = SpecialDetail(displayName: dto.displayName,
                                   originalPrice: dto.originalPrice,
                                   price: dto.price)
        let ui = SpecialUI(size: size,
                           image: image)
        self.init(ui: ui, detail: detail, listIndex: dto.listIndex)
    }
}
