//
//  SpecialDTO+JSON.swift
//  ProductSpecials
//
//  Created by Andrew Bush on 15/05/21.
//

import Foundation

extension SpecialDTO {
    init?(json: Dictionary<String, Any>, listIndex: Int) {
        guard let imageURL = json["imageUrl"] as? String else {
            return nil
        }
        guard let height = json["height"] as? Int else {
            return nil
        }
        guard let width = json["width"] as? Int else {
            return nil
        }
        guard let displayName = json["display_name"] as? String else {
            return nil
        }
        guard let originalPrice = json["original_price"] as? String else {
            return nil
        }
        guard let price = json["price"] as? String else {
            return nil
        }
        self.listIndex = listIndex
        self.imageURL = imageURL
        self.width = width
        self.height = height
        self.displayName = displayName
        self.originalPrice = originalPrice
        self.price = price
    }
}
