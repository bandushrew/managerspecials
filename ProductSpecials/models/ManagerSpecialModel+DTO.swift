//
//  ManagerSpecialModel+DTO.swift
//  ProductSpecials
//
//  Created by Andrew Bush on 15/05/21.
//

import Foundation
import UIKit
extension ManagerSpecialModel {
    init(viewWidth: CGFloat, dto: ManagerSpecialDTO, imageCache: Dictionary<String, UIImage>) {
        self.canvasUnit = dto.canvasUnit
        var convertedModels: [SpecialModel] = []
        for specialDTO in dto.specials {
            let size = ManagerSpecialModel.frame(from: specialDTO,
                                                 canvasUnit: canvasUnit,
                                                 viewWidth: viewWidth)
            if let special = SpecialModel(dto: specialDTO,
                                          size: size,
                                          imageCache: imageCache) {
                convertedModels.append(special)
            }
        }
        self.specials = convertedModels
    }
}
