//
//  ManagerSpecialResponse.swift
//  ProductSpecials
//
//  Created by Andrew Bush on 13/05/21.
//

import Foundation

struct ManagerSpecialDTO {
    let canvasUnit: Int
    let specials: [SpecialDTO]
}
