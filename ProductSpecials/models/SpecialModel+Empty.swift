//
//  SpecialModel+Empty.swift
//  ProductSpecials
//
//  Created by Rentamac on 18/05/21.
//

import Foundation
import UIKit

extension SpecialModel {
    init() {
        let detail = SpecialDetail(displayName: "",
                                   originalPrice: "",
                                   price: "")
        let ui = SpecialUI(size: CGSize(),
                           image: UIImage())
        self.init(ui: ui, detail: detail, listIndex: 0)
    }
}
