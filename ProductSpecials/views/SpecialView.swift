//
//  SpecialView.swift
//  ProductSpecials
//
//  Created by Andrew Bush on 14/05/21.
//

import Foundation
import SwiftUI

struct SpecialView: View {
    @Environment(\.standardSpacing) var standardSpacing
    @Environment(\.borderColor) var borderColor
    @Environment(\.borderWidth) var borderWidth
    @Environment(\.cornerRadius) var cornerRadius
    @Environment(\.cardBackground) var cardBackground
    
    let special: SpecialModel
    let paddingTrailing: CGFloat
    let paddingLeading: CGFloat
    let adjustmentPaddingVertical: CGFloat = 0.1
    let adjustmentPaddingHorizontal: CGFloat = 0.04

    var body: some View {
        HStack {
                VStack(alignment: .center, spacing: standardSpacing, content: {
                    VStack {
                    HStack(alignment: .center, spacing: standardSpacing, content: {
                        SpecialImage(special: special)
                        Spacer(minLength: 1)
                        VStack(alignment: .trailing, spacing: standardSpacing, content: {
                            OldPriceText(special: special)
                            SpecialPriceText(special: special)

                        })
                    })
                    DisplayNameText(special: special)
                    }
                    .padding(.top, special.ui.size.height * adjustmentPaddingVertical)
                    .padding(.bottom, special.ui.size.height * adjustmentPaddingVertical)
                    .padding(.leading, special.ui.size.height * adjustmentPaddingHorizontal)
                    .padding(.trailing, special.ui.size.height * adjustmentPaddingHorizontal)
                })
                .background(cardBackground)
                .border(borderColor, width: borderWidth)
                .cornerRadius(cornerRadius)
                .overlay(
                    RoundedRectangle(cornerRadius: cornerRadius)
                        .stroke(lineWidth: borderWidth)
                        .foregroundColor(borderColor)
                  )
                .padding(.leading, paddingLeading)
                .padding(.trailing, paddingTrailing)
                .padding(.top, 10)
                .padding(.bottom, 10)
                .shadow(radius: 10, x:0, y:20)
        }.frame(width: special.ui.size.width,
                height: special.ui.size.height,
                alignment: .center)
       
    }
}

struct SpecialView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            SpecialView(special: PreviewData.preview2(),
                        paddingTrailing: 10,
                        paddingLeading: 10)
            SpecialView(special: PreviewData.preview1(),
                        paddingTrailing: 10,
                        paddingLeading: 10)
            SpecialView(special: PreviewData.preview3(),
                        paddingTrailing: 10,
                        paddingLeading: 10)
        }
    }
}
