//
//  DisplayNameText.swift
//  ProductSpecials
//
//  Created by Andrew Bush on 16/05/21.
//

import Foundation
import SwiftUI

struct DisplayNameText: View {
    let special: SpecialModel

    var body: some View {
        Text(special.detail.displayName)
            .frame(minWidth: 0, idealWidth: special.ui.size.width/2, maxWidth: special.ui.size.width/4*3,
                   minHeight: 0, idealHeight: special.ui.size.height/3, maxHeight: .infinity,
                   alignment: .center)
            .font(.title3)
            .multilineTextAlignment(.center)
            .minimumScaleFactor(0.2)
            .lineLimit(2)
    }
}
