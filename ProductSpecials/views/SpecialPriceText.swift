//
//  SpecialPriceText.swift
//  ProductSpecials
//
//  Created by Andrew Bush on 16/05/21.
//

import Foundation
import SwiftUI

struct SpecialPriceText: View {
    @Environment(\.specialPriceColor) var specialPriceColor
    
    let special: SpecialModel
    let adjustmentTextPrice: CGFloat = 0.15

    var body: some View {
        Text("$"+special.detail.price)
            .foregroundColor(specialPriceColor)
            .font(.system(size: special.ui.size.height * adjustmentTextPrice))
            .fontWeight(.bold)
            .minimumScaleFactor(0.1)
            .lineLimit(1)
    }
}
