//
//  SpecialViewHStack.swift
//  ProductSpecials
//
//  Created by Andrew Bush on 14/05/21.
//

import Foundation
import SwiftUI

struct SpecialViewHStack: View {
    @Environment(\.standardSpacing) var standardSpacing

    let specials: [SpecialModel]
    var body: some View {
        LazyHStack(alignment: .center,
                   spacing: standardSpacing,
                   content: {
                    ForEach(specials) { special in
                        SpecialView(special: special,
                                    paddingTrailing: paddingTrailing(special: special, specials: specials),
                                    paddingLeading: paddingLeading(special: special, specials: specials))
                    }
                   }).padding(0)
    }
    
    func paddingTrailing(special: SpecialModel, specials: [SpecialModel]) -> CGFloat {
        if specials.count == 1 {
            return 10
        }
        if(specials.first == special) {
            return 5
        }
        if(specials.last == special) {
            return 10
        }
        return 5
    }
    func paddingLeading(special: SpecialModel, specials: [SpecialModel]) -> CGFloat {
        if specials.count == 1 {
            return 10
        }
        if(specials.first == special) {
            return 10
        }
        if(specials.last == special) {
            return 5
        }
        return 5
    }
    
}
