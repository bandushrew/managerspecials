//
//  SpecialImage.swift
//  ProductSpecials
//
//  Created by Andrew Bush on 16/05/21.
//

import Foundation
import SwiftUI

struct SpecialImage: View {
    let special: SpecialModel
    let adjustmentImage: CGFloat = 0.4

    var body: some View {
        Image(uiImage: special.ui.image)
            .resizable()
            .aspectRatio(contentMode: .fit)
            .frame(minWidth: 10, idealWidth: special.ui.size.height * adjustmentImage,
                   maxWidth: special.ui.size.height * adjustmentImage,
                   minHeight: 10, idealHeight: special.ui.size.height * adjustmentImage,
                   maxHeight: special.ui.size.height * adjustmentImage,
                   alignment: .center)
           
    }
}
