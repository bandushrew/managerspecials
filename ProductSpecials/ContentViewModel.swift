//
//  ContentViewModel.swift
//  ProductSpecials
//
//  Created by Andrew Bush on 15/05/21.
//

import SwiftUI
import Combine

enum LoadingState {
    case loading
    case loaded
    case error(message: String)
}

class ContentViewModel: ObservableObject {
    var dataProvider: DataProviderProtocol
    
    enum Constants {
        enum urls: String {
            case dataURL = "https://raw.githubusercontent.com/Swiftly-Systems/code-exercise-ios/master/backup"
        }
        enum timer: Double {
            case repeatSeconds = 300
        }
    }

    @Published private(set) var state: LoadingState
    func orderedSpecials(viewWidth: CGFloat) -> [ModelContainer] {
        if let dataLoaderResult = self.dataLoaderResult {
            let managerSpecials = ManagerSpecialModel(viewWidth: UIScreen.main.bounds.width,
                                                      dto: dataLoaderResult.dto,
                                                      imageCache: dataLoaderResult.imageCache)
            return managerSpecials.orderedSpecials(viewWidth: UIScreen.main.bounds.width)
        }
        return []
    }

    private var timer: Timer?
    private var dataLoaderResult: DataLoaderResult?
        
    private func loadContent() {
        self.dataProvider.load(delegate: self,
                               urlString: Constants.urls.dataURL.rawValue)
    }

    func reload() {
        self.state = .loading
        self.loadContent()
    }
    
    init(provider: DataProviderProtocol, initialState: LoadingState) {
        self.dataProvider = provider
        self.state = initialState
        loadContent()
        
        timer = Timer.scheduledTimer(withTimeInterval: Constants.timer.repeatSeconds.rawValue,
                                     repeats: true) { [weak self] timer in
            self?.loadContent()
        }
    }
}

extension ContentViewModel: DataLoaderDelegate {
    func didComplete(result: Result<DataLoaderResult, Error>) {
        switch result {
        case .success(let dataLoaderResult):
            self.dataLoaderResult = dataLoaderResult
            // It was returning too quickly after a pull to refresh, the UI looked odd, so Im adding a short delay
            // So the user knows they did a thing
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.state = .loaded
            }
        case .failure(let error):
            DispatchQueue.main.async {
                self.state = .error(message: error.localizedDescription)
            }
        }
    }
}
