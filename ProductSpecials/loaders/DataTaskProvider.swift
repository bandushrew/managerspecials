//
//  DataTaskProvider.swift
//  ProductSpecials
//
//  Created by Andrew Bush on 16/05/21.
//

import Foundation

protocol DataLoaderProviderProtocol {
    func provide(url: URL,
                 completion: @escaping(_ data: Data?, _ response: URLResponse?, _ error: Error?) -> Void)
}
struct DataTaskProvider: DataLoaderProviderProtocol {
    func provide(url: URL, completion: @escaping(_ data: Data?, _ response: URLResponse?, _ error: Error?) -> Void) {
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
        }
        task.resume()
    }
}
