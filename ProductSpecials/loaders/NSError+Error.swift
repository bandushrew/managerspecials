//
//  NSError+Error.swift
//  ProductSpecials
//
//  Created by Andrew Bush on 17/05/21.
//

import Foundation

extension NSError {
    static func ProductSpecialError(message: String) -> Error {
        let userInfo = [NSLocalizedDescriptionKey : message]
        let nsError = NSError(domain: "ProductSpecials",
                              code: 10999,
                              userInfo: userInfo)
     
        return nsError as Error
    }
}
