//
//  ImageLoader.swift
//  ProductSpecials
//
//  Created by Andrew Bush on 15/05/21.
//

import Foundation
import UIKit

protocol JSONLoaderDelegate: AnyObject {
    func didComplete(result: Result<Any, Error>)
}
class JSONLoader {
    let urlLoaderProvider: DataLoaderProviderProtocol
    private weak var delegate: JSONLoaderDelegate?
    
    init(urlLoaderProvider: DataLoaderProviderProtocol) {
        self.urlLoaderProvider = urlLoaderProvider
    }
    
    func loadJSON(urlString: String, delegate: JSONLoaderDelegate) {
        self.delegate = delegate
        if let url = URL(string: urlString) {
            self.urlLoaderProvider.provide(url: url) { [weak self] data, response, error in
                guard let delegate = self?.delegate else {
                    return
                }
                if let error = error {
                    delegate.didComplete(result: .failure(error))
                    return
                }
                guard let data = data else {
                    let error = NSError.ProductSpecialError(message: "No data retrieved")
                    delegate.didComplete(result: .failure(error))
                    return
                }

                do {
                    let json =  try JSONSerialization.jsonObject(with: data)
                    delegate.didComplete(result: .success(json))
                } catch {
                    delegate.didComplete(result: .failure(error))
                }
            }
        }
    }
}
