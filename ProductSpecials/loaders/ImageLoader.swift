//
//  ImageLoader.swift
//  ProductSpecials
//
//  Created by Andrew Bush on 15/05/21.
//

import Foundation
import UIKit

protocol ImageLoaderDelegate: AnyObject {
    func didComplete(result: Result<Dictionary<String, UIImage>, Error>)
}

class ImageLoader {
    let urlLoaderProvider: DataLoaderProviderProtocol
    private var imageCache: Dictionary<String, UIImage> = [:]
    private var urlsRequested: Dictionary<String, Bool> = [:]
    private weak var delegate: ImageLoaderDelegate?
    
    init(urlLoaderProvider: DataLoaderProviderProtocol) {
        self.urlLoaderProvider = urlLoaderProvider
    }
    
    func loadImage(urlString: String, delegate: ImageLoaderDelegate) {
        self.delegate = delegate
        if let url = URL(string: urlString) {
            urlsRequested[urlString] = false
            self.urlLoaderProvider.provide(url: url) { [weak self] data, response, error in
                guard let strongSelf = self, let delegate = strongSelf.delegate else {
                    return
                }
                if let error = error {
                    delegate.didComplete(result: .failure(error))
                    return
                }
                guard let data = data else {
                    let error = NSError.ProductSpecialError(message: "No data retrieved")
                    delegate.didComplete(result: .failure(error))
                    return
                }
                strongSelf.imageCache[urlString] = UIImage(data: data)
                strongSelf.urlsRequested[urlString] = true
                if(strongSelf.urlsRequested.values.contains(false) == false) {
                    delegate.didComplete(result: .success(strongSelf.imageCache))
                }
            }
        }
    }
}
