//
//  JSONFileLoader.swift
//  ProductSpecials
//
//  Created by Andrew Bush on 15/05/21.
//

import Foundation
import UIKit

class PreviewLoader: DataProviderProtocol {
    func load(delegate: DataLoaderDelegate, urlString: String) {
        let successResult = PreviewData.dataLoaderResult()
        delegate.didComplete(result: .success(successResult))
    }
}
