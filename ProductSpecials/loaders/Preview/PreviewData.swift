//
//  SpecialViewPreviewData.swift
//  ProductSpecials
//
//  Created by Andrew Bush on 16/05/21.
//

import Foundation
import UIKit
struct PreviewData {
    static func dataLoaderResult() -> DataLoaderResult {
        var imageCache: Dictionary<String, UIImage> = [:]
        let managerSpecialDTO = PreviewData.managerSpecialDTO()
        
        for specialDTO in managerSpecialDTO.specials {
            imageCache[specialDTO.imageURL] = UIImage(named: "L")
        }
        
        return DataLoaderResult(dto: managerSpecialDTO,
                                imageCache: imageCache)
    }
    
    private static func managerSpecialDTO() -> ManagerSpecialDTO {
        let managerSpecialDTO = ManagerSpecialDTO(canvasUnit: 16,
                                 specials: PreviewData.specialDTOs())
        return managerSpecialDTO
    }
    
    private static func specialDTOs() -> [SpecialDTO] {
        return [
            SpecialDTO(imageURL: "https://raw.githubusercontent.com/Swiftly-Systems/code-exercise-ios/master/images/L.png",
                                        width: 16,
                                        height: 8,
                                        displayName: "Noodle Dish with Roasted Black Bean Sauce",
                                        originalPrice: "2.00",
                                        price: "1.00",
                                        listIndex: 0),
            SpecialDTO(imageURL: "https://raw.githubusercontent.com/Swiftly-Systems/code-exercise-ios/master/images/L.png",
                                        width: 8,
                                        height: 8,
                                        displayName: "Onion Flavored Rings",
                                        originalPrice: "2.00",
                                        price: "1.00",
                                        listIndex: 1),
            SpecialDTO(imageURL: "https://raw.githubusercontent.com/Swiftly-Systems/code-exercise-ios/master/images/L.png",
                                        width: 8,
                                        height: 8,
                                        displayName: "Kikkoman Less Sodium Soy Sauce",
                                        originalPrice: "2.00",
                                        price: "1.00",
                                        listIndex: 2)
        ]
    }
    static func preview1() -> SpecialModel {
        let specialDTO = SpecialDTO(imageURL: "https://raw.githubusercontent.com/Swiftly-Systems/code-exercise-ios/master/images/L.png",
                                    width: 16,
                                    height: 8,
                                    displayName: "Noodle Dish with Roasted Black Bean Sauce",
                                    originalPrice: "2.00",
                                    price: "1.00",
                                    listIndex: 0)
     
        var imageCache:Dictionary<String, UIImage> = [:]
        imageCache["https://raw.githubusercontent.com/Swiftly-Systems/code-exercise-ios/master/images/L.png"] = UIImage(named: "L")
        
        let size=ManagerSpecialModel.frame(from: specialDTO,
                                           canvasUnit: 16,
                                           viewWidth: UIScreen.main.bounds.width)
        let special = SpecialModel(dto: specialDTO,
                                   size: size,
                                   imageCache: imageCache)
        
        return special ?? SpecialModel()
    }
    
    static func preview2() -> SpecialModel {
        let specialDTO = SpecialDTO(imageURL: "https://raw.githubusercontent.com/Swiftly-Systems/code-exercise-ios/master/images/L.png",
                                    width: 8,
                                    height: 8,
                                    displayName: "Onion Flavored Rings",
                                    originalPrice: "2.00",
                                    price: "1.00",
                                    listIndex: 0)
     
        var imageCache:Dictionary<String, UIImage> = [:]
        imageCache["https://raw.githubusercontent.com/Swiftly-Systems/code-exercise-ios/master/images/L.png"] = UIImage(named: "L")
        
        let size=ManagerSpecialModel.frame(from: specialDTO,
                                           canvasUnit: 16,
                                           viewWidth: UIScreen.main.bounds.width)
        let special = SpecialModel(dto: specialDTO,
                                   size: size,
                                   imageCache: imageCache)
        
        return special ?? SpecialModel()
    }

    static func preview3() -> SpecialModel {
        let specialDTO = SpecialDTO(imageURL: "https://raw.githubusercontent.com/Swiftly-Systems/code-exercise-ios/master/images/L.png",
                                    width: 8,
                                    height: 8,
                                    displayName: "Kikkoman Less Sodium Soy Sauce",
                                    originalPrice: "2.00",
                                    price: "1.00",
                                    listIndex: 0)
     
        var imageCache:Dictionary<String, UIImage> = [:]
        imageCache["https://raw.githubusercontent.com/Swiftly-Systems/code-exercise-ios/master/images/L.png"] = UIImage(named: "L")
        
        let size=ManagerSpecialModel.frame(from: specialDTO,
                                           canvasUnit: 16,
                                           viewWidth: UIScreen.main.bounds.width)
        let special = SpecialModel(dto: specialDTO,
                                   size: size,
                                   imageCache: imageCache)
        
        return special ?? SpecialModel()
    }
}
