//
//  DataLoader.swift
//  ProductSpecials
//
//  Created by Andrew Bush on 15/05/21.
//

import Foundation
import UIKit

protocol DataLoaderDelegate: AnyObject {
    func didComplete(result: Result<DataLoaderResult, Error>)
}

struct DataLoaderResult {
    let dto: ManagerSpecialDTO
    let imageCache: Dictionary<String, UIImage>
}

protocol DataProviderProtocol {
    func load(delegate: DataLoaderDelegate, urlString: String)
}

class DataLoader: DataProviderProtocol {
    let urlLoaderProvider: DataLoaderProviderProtocol
    
    private var imageLoader: ImageLoader?
    private var jsonLoader: JSONLoader?
    private var managerSpecialDTO: ManagerSpecialDTO?
    private weak var delegate: DataLoaderDelegate?;
    
    init(urlLoaderProvider: DataLoaderProviderProtocol) {
        self.urlLoaderProvider = urlLoaderProvider
    }
    
    func load(delegate: DataLoaderDelegate, urlString: String) {
        self.jsonLoader = JSONLoader(urlLoaderProvider:self.urlLoaderProvider)
        self.imageLoader = ImageLoader(urlLoaderProvider:self.urlLoaderProvider)
        self.delegate = delegate
        self.jsonLoader?.loadJSON(urlString: urlString,
                                 delegate: self)
    }
    
    private func loadImages() {
        if let managerDTO = self.managerSpecialDTO  {
            if managerDTO.specials.count == 0 {
                self.completeSuccess(imageCache: [:])
            } else {
                for special in managerDTO.specials {
                    self.imageLoader?.loadImage(urlString: special.imageURL,
                                                delegate: self)
                }
            }
            return
        }
    }
        
    private func completeSuccess(imageCache: Dictionary<String, UIImage>) {
        if let managerDTO = self.managerSpecialDTO {
            let successResult = DataLoaderResult(dto: managerDTO, imageCache: imageCache)
            self.delegate?.didComplete(result: .success(successResult))
        }
    }
}

extension DataLoader: JSONLoaderDelegate {
    func didComplete(result: Result<Any, Error>) {
        switch result {
        case .success(let json):
        if let rawJSON = json as? Dictionary<String, Any> {
            self.managerSpecialDTO = ManagerSpecialDTO(json: rawJSON)
            self.loadImages()
        }
        case .failure(let error):
            self.delegate?.didComplete(result: .failure(error))
        }
    }
}

extension DataLoader: ImageLoaderDelegate {
    func didComplete(result: Result<Dictionary<String, UIImage>, Error>) {
        switch result {
        case .success(let imageCache):
            self.completeSuccess(imageCache: imageCache)
        case .failure(let error):
            self.delegate?.didComplete(result: .failure(error))
        }

    }
}
