//
//  ContentViewEnvironment.swift
//  ProductSpecials
//
//  Created by Andrew Bush on 16/05/21.
//

import Foundation
import SwiftUI
private struct ContentViewBackgroundColor: EnvironmentKey {
    static let defaultValue = Color(.systemGray5).opacity(0.5)
}
private struct ContentViewBorderColor: EnvironmentKey {
    static let defaultValue = Color(.lightGray).opacity(0.5)
}

private struct ContentViewBorderWidth: EnvironmentKey {
    static let defaultValue: CGFloat = 1
}

private struct ContentViewCornerRadius: EnvironmentKey {
    static let defaultValue: CGFloat = 10
}

private struct ContentViewCardBackground: EnvironmentKey {
    static let defaultValue = Color(.white)
}

private struct ContentViewStandardSpacing: EnvironmentKey {
    static let defaultValue:CGFloat = 0
}

private struct ContentViewOldPriceColor: EnvironmentKey {
    static let defaultValue = Color(.gray)
}

private struct ContentViewSpecialPriceColor: EnvironmentKey {
    static let defaultValue = Color(.systemGreen)
}

extension EnvironmentValues {
    var backgroundColor: Color {
        get { self[ContentViewBackgroundColor.self] }
        set { self[ContentViewBackgroundColor.self] = newValue }
    }
    
    var borderColor: Color {
        get { self[ContentViewBorderColor.self] }
        set { self[ContentViewBorderColor.self] = newValue }
    }
    
    var borderWidth: CGFloat {
        get { self[ContentViewBorderWidth.self] }
        set { self[ContentViewBorderWidth.self] = newValue }
    }
    
    var cornerRadius: CGFloat {
        get { self[ContentViewCornerRadius.self] }
        set { self[ContentViewCornerRadius.self] = newValue }
    }
    
    var cardBackground: Color {
        get { self[ContentViewCardBackground.self] }
        set { self[ContentViewCardBackground.self] = newValue }
    }
    
    var standardSpacing: CGFloat {
        get { self[ContentViewStandardSpacing.self] }
        set { self[ContentViewStandardSpacing.self] = newValue }
    }
    
    var oldPriceColor: Color {
        get { self[ContentViewOldPriceColor.self] }
        set { self[ContentViewOldPriceColor.self] = newValue }
    }
    
    var specialPriceColor: Color {
        get { self[ContentViewSpecialPriceColor.self] }
        set { self[ContentViewSpecialPriceColor.self] = newValue }
    }
}
