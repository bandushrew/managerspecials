//
//  ContentView.swift
//  ProductSpecials
//
//  Created by Andrew Bush on 13/05/21.
//

import SwiftUI

struct ContentView: View {
    @Environment(\.backgroundColor) var backgroundColor
    @Environment(\.standardSpacing) var standardSpacing
    @EnvironmentObject var viewModel: ContentViewModel
    
    var gridItemLayout = [GridItem()]

    var body: some View {
        ZStack {
            backgroundColor.edgesIgnoringSafeArea(.vertical)
            VStack {
                Text("Manager's Specials")
                    .font(.title3)
                    .padding(.top, 10)
                switch viewModel.state {
                    case .loading:
                        Text("Loading...").font(.title3)
                        ProgressView()
                    case .error(let message):
                        Text(message).foregroundColor(.red).font(.largeTitle)
                    case .loaded:
                        GeometryReader { geometry in
                        ScrollView {
                            LazyVGrid(columns: gridItemLayout, spacing: standardSpacing) {
                                ForEach(viewModel.orderedSpecials(viewWidth: UIScreen.main.bounds.width)) { container in
                                    SpecialViewHStack(specials: container.productSpecials)
                                }
                            }.anchorPreference(key: OffsetPreferenceKey.self, value: .top) {
                                geometry[$0].y
                            }
                        }
                        .onPreferenceChange(OffsetPreferenceKey.self) { offset in
                            if offset > 60 {
                                viewModel.reload()
                            }
                    }
                    }
                }
            }
        }                        .edgesIgnoringSafeArea(.horizontal)

    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ContentView()
                .environmentObject(
                ContentViewModel(provider: PreviewLoader(),
                                                                        
                                            initialState: .loaded))
            
            ContentView()
                .environmentObject(ContentViewModel(provider: PreviewLoader(),
                                                             initialState: .loading))
            
            ContentView()
                .environmentObject(ContentViewModel(provider: PreviewLoader(),
                                                             initialState: .error(message: "Its not working!")))
        }
    }
}


fileprivate struct OffsetPreferenceKey: PreferenceKey {
    static var defaultValue: CGFloat = 0
    
    static func reduce(value: inout CGFloat, nextValue: () -> CGFloat) {
        value = nextValue()
    }
}
