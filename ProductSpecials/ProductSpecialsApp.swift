//
//  ProductSpecialsApp.swift
//  ProductSpecials
//
//  Created by Andrew Bush on 13/05/21.
//

import SwiftUI

@main
struct ProductSpecialsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView().environmentObject(ContentViewModel(provider: DataLoader(urlLoaderProvider: DataTaskProvider()), initialState: .loading))
        }
    }
}
