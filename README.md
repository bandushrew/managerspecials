# README #
### What is this repository for? ###

This has been written as a coding exercise to submit to Swiftly

https://github.com/Swiftly-Systems/code-exercise-ios/blob/master/README.md



### How do I get set up? ###

This project has no additional dependencies, just open the project file in XCode and choose Product->Run

### Who do I talk to? ###

This code was written by Andrew Bush
