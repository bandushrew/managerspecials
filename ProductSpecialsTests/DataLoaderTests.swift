//
//  DataLoaderTests.swift
//  ProductSpecialsTests
//
//  Created by Andrew Bush on 17/05/21.
//

import XCTest
@testable import ProductSpecials

class DataLoaderTests: XCTestCase {
    class MockDelegate: DataLoaderDelegate {
        var error: Error?
        var managerSpecials: ManagerSpecialModel?
        func didComplete(result: Result<DataLoaderResult, Error>) {
            switch result {
            case.failure(let error):
                    self.error = error
            case .success(let dataLoaderResult):
                self.managerSpecials = ManagerSpecialModel(viewWidth: UIScreen.main.bounds.width,
                                                           dto: dataLoaderResult.dto,
                                                           imageCache: dataLoaderResult.imageCache)
            }
        }
    }
    
    func testLoadCallsDidCompleteOnError() throws {
        struct MockDataTaskProvider: DataLoaderProviderProtocol {
            func provide(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
                let error = NSError.ProductSpecialError(message: "test")
                completion(nil, nil, error)
            }
        }
        
        let dataLoader = DataLoader(urlLoaderProvider: MockDataTaskProvider())
        let delegate = MockDelegate()
        dataLoader.load(delegate: delegate, urlString: "https://www.google.com")
        XCTAssertNotNil(delegate.error)
    }
    
    func testLoadCallsDidCompleteOnSuccess() throws {
        class MockDataTaskProvider: DataLoaderProviderProtocol {
            func provide(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
                let data = "{\"canvasUnit\":16,\"managerSpecials\":[]}".data(using: .utf8)
                completion(data, nil, nil)
            }
        }
        
        let dataLoader = DataLoader(urlLoaderProvider: MockDataTaskProvider())
        let delegate = MockDelegate()
        dataLoader.load(delegate: delegate, urlString: "https://www.google.com")
        XCTAssertNotNil(delegate.managerSpecials)
    }
}

