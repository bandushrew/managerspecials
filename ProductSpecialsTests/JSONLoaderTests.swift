//
//  JSONLoaderTests.swift
//  ProductSpecialsTests
//
//  Created by Andrew Bush on 17/05/21.
//

import XCTest
@testable import ProductSpecials

class JSONLoaderTests: XCTestCase {
    class MockDelegate: JSONLoaderDelegate {
        var error: Error?
        var json: Any?
        func didComplete(result: Result<Any, Error>) {
            switch result {
            case.failure(let error):
                    self.error = error
            case .success(let json):
                self.json = json
            }
        }
    }
    
    func testLoadCallsDidCompleteOnError() throws {
        struct MockDataTaskProvider: DataLoaderProviderProtocol {
            func provide(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
                let error = NSError.ProductSpecialError(message: "test")
                completion(nil, nil, error)
            }
        }
        
        let imageLoader = JSONLoader(urlLoaderProvider: MockDataTaskProvider())
        let delegate = MockDelegate()
        imageLoader.loadJSON(urlString: "https://www.google.com", delegate: delegate)
        XCTAssertNotNil(delegate.error)
    }
    
    func testLoadCallsDidCompleteOnSuccess() throws {
        struct MockDataTaskProvider: DataLoaderProviderProtocol {
            func provide(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
                let data = "{\"p\":\"p\"}".data(using: .utf8)
                completion(data, nil, nil)
            }
        }
        
        let jsonLoader = JSONLoader(urlLoaderProvider: MockDataTaskProvider())
        let delegate = MockDelegate()
        jsonLoader.loadJSON(urlString: "https://www.google.com", delegate: delegate)
        XCTAssertNotNil(delegate.json)
    }
}
