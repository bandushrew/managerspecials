//
//  ImageLoaderTests.swift
//  ProductSpecialsTests
//
//  Created by Andrew Bush on 15/05/21.
//

import XCTest
@testable import ProductSpecials

class ImageLoaderTests: XCTestCase {
    class MockDelegate: ImageLoaderDelegate {
        var error: Error?
        var imageCache: Dictionary<String, UIImage>?
        func didComplete(result: Result<Dictionary<String, UIImage>, Error>) {
            switch result {
            case.failure(let error):
                    self.error = error
            case .success(let imageCache):
                self.imageCache = imageCache
            }
        }
    }
    
    func testLoadCallsDidCompleteOnError() throws {
        struct MockDataTaskProvider: DataLoaderProviderProtocol {
            func provide(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
                let error = NSError.ProductSpecialError(message: "test")
                completion(nil, nil, error)
            }
        }
        
        let imageLoader = ImageLoader(urlLoaderProvider: MockDataTaskProvider())
        let delegate = MockDelegate()
        imageLoader.loadImage(urlString: "https://www.google.com", delegate: delegate)
        XCTAssertNotNil(delegate.error)
    }
    
    func testLoadCallsDidCompleteOnSuccess() throws {
        struct MockDataTaskProvider: DataLoaderProviderProtocol {
            func provide(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
                let image = UIImage(systemName: "keyboard")
                let data = image?.pngData()
                completion(data, nil, nil)
            }
        }
        
        let imageLoader = ImageLoader(urlLoaderProvider: MockDataTaskProvider())
        let delegate = MockDelegate()
        imageLoader.loadImage(urlString: "https://www.google.com", delegate: delegate)
        XCTAssertNotNil(delegate.imageCache)
        XCTAssertTrue(delegate.imageCache!["https://www.google.com"] != nil)
    }
}
