//
//  FrameCalculatorTests.swift
//  ProductSpecialsTests
//
//  Created by Andrew Bush on 13/05/21.
//

import XCTest
@testable import ProductSpecials

class ManagerSpecialModelTests: XCTestCase {
    func testFrameWithGivenExampleFromSpecification() throws {
        let viewWidth = CGFloat(360)
        let canvasUnit = 8
        let specialDTO=SpecialDTO(imageURL: "imageURL",
                                  width: 6,
                                  height: 3,
                                  displayName: "displayName",
                                  originalPrice: "originalPrice",
                                  price: "price",
        listIndex: 0)
        
        let size = ManagerSpecialModel.frame(from: specialDTO,
                                             canvasUnit: canvasUnit,
                                             viewWidth: viewWidth)
        XCTAssertTrue(size.width==270)
        XCTAssertTrue(size.height==135)
    }
    
    func testsOrderedSpecialsArrangesThemCorrectly() throws {
        let loaderResult = PreviewData.dataLoaderResult()
        let managerSpecial = ManagerSpecialModel(viewWidth: 360,
                                                 dto: loaderResult.dto,
                                                 imageCache: loaderResult.imageCache)
        let orderedSpecials = managerSpecial.orderedSpecials(viewWidth: 360)
        XCTAssertTrue(orderedSpecials[0].productSpecials.count == 1)
        XCTAssertTrue(orderedSpecials[1].productSpecials.count == 2)
    }
    
    func testsOrderedSpecialsMaintainsOriginalListIndec() throws {
        let loaderResult = PreviewData.dataLoaderResult()
        let managerSpecial = ManagerSpecialModel(viewWidth: 360,
                                                 dto: loaderResult.dto,
                                                 imageCache: loaderResult.imageCache)
        let orderedSpecials = managerSpecial.orderedSpecials(viewWidth: 360)
        XCTAssertTrue(orderedSpecials[0].productSpecials[0].listIndex == 0)
        XCTAssertTrue(orderedSpecials[1].productSpecials[0].listIndex == 1)
        XCTAssertTrue(orderedSpecials[1].productSpecials[1].listIndex == 2)
    }
}
