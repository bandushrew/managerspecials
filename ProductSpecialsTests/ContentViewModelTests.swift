//
//  ContentViewModelTests.swift
//  ProductSpecialsTests
//
//  Created by Andrew Bush on 17/05/21.
//

import XCTest
import Combine
@testable import ProductSpecials

class ContentViewModelTests: XCTestCase {
    func testSetsStateToErrorWhenErrorOccurs() throws {
        let expectation = self.expectation(description: "Wait for state change")

        let model = ContentViewModel(provider: PreviewLoader(),
                                     initialState: .loading)
        // XCode suggests changing subscriber to _ in a warning
        // but if we do that, we dont receive the published events (expectation fails)
        // so am ignoring and adding a usage of subscriber to stop xcode complaining
        var subscriberArray: [Any] = []
        let subscriber = model.$state
            .sink { state in
            switch state {
            case .error:
                expectation.fulfill()
            default:
                print(state)
            }
        }
        subscriberArray.append(subscriber)
        
        let error = NSError.ProductSpecialError(message: "test")
        model.didComplete(result: .failure(error))
        wait(for: [expectation], timeout: 2)
        switch model.state {
        case .error:
            XCTAssertTrue(true)
        default:
            XCTAssertTrue(false)
        }
    }
    
    func testSetsStateToLoadedWhenLoadingCompleted() throws {
        let expectation = self.expectation(description: "Wait for state change")
        expectation.assertForOverFulfill = false
        let model = ContentViewModel(provider: PreviewLoader(),
                                     initialState: .loading)
        // XCode suggests changing subscriber to _ in a warning
        // but if we do that, we dont receive the published events (expectation fails)
        // so am ignoring and adding a usage of subscriber to stop xcode complaining
        var subscriberArray: [Any] = []
        let subscriber = model.$state
            .sink { state in
            switch state {
            case .loaded:
                expectation.fulfill()
            default:
                print(state)
            }
        }
        subscriberArray.append(subscriber)
        
        let dataLoaderResult = PreviewData.dataLoaderResult()
        model.didComplete(result: .success(dataLoaderResult))
        wait(for: [expectation], timeout: 2)
        switch model.state {
        case .loaded:
            XCTAssertTrue(true)
        default:
            XCTAssertTrue(false)
        }
    }
    
    func testSetsStateToLoadingWhenReloadUsed() throws {
        let model = ContentViewModel(provider: PreviewLoader(),
                                     initialState: .error(message: "oops"))
        model.reload()
        switch model.state {
        case .loading:
            XCTAssertTrue(true)
        default:
            XCTAssertTrue(false)
        }
    }
}
